package com.company;

import java.util.Scanner;

public class ScanOptions {

    //Colors for the console text
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_WHITE = "\u001B[37m";
    // Colors for the console background
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    // Some pre-made coloring options
    public static final String ANSI_Fill = ANSI_BLACK_BACKGROUND+ANSI_BLACK;
    public static final String ANSI_Wrong = ANSI_RED_BACKGROUND+ANSI_BLACK;
    public static final String ANSI_Usertext = ANSI_WHITE_BACKGROUND + ANSI_BLACK;
    public static final String ANSI_FinalResult = ANSI_BLACK_BACKGROUND + ANSI_YELLOW;
    public static final String ANSI_Correct = ANSI_GREEN_BACKGROUND + ANSI_BLACK;


    /**
     * <Strong>This is used for inputs from the user, it prints a text and then prompts the user for an answer</Strong>
     * @return This method returns the string, which the user inputted via the scanner
     * @param headline The text you want to print before the userpromt
     * @param minLength The minimum length the string can be, otherwise it will not be accepted
     * @param maxLength The maximum lenght the string can be, otherwise it will not be accepted
     */
    public static String selectString(String headline, int minLength, int maxLength){

        Scanner choice = new Scanner(System.in);
        String result="";
        boolean runAtLeastOnceBefore=false;

        //This is a failsafe, so the input to the sql server doesn't get to long for the containers. (It is a checker & repeater with 2 error messages)
        do {
            if (result.length()>maxLength && runAtLeastOnceBefore){
                System.out.println(ANSI_Wrong+"ERROR: String can't be longer then "+maxLength+" chars");
            }
            if (result.length()<minLength && runAtLeastOnceBefore){
                System.out.println(ANSI_Wrong+"ERROR: String can't be shorter then "+minLength+" chars");
            }
            System.out.print(ANSI_Usertext+headline+": ");
            System.out.print(ANSI_RESET+" ");
            result=choice.nextLine();
            System.out.println(ANSI_Fill+"");
            runAtLeastOnceBefore=true;
        } while (result.length()>maxLength || result.length()<minLength);

        return result;
    }

    /**
     * <Strong>This is used for inputs from the user, it prints a text and then prompts the user for an answer</Strong>
     * @return This method returns the int, which the user inputted via the scanner
     * @param headline The text you want to print before the userprompt
     * @param minValue The minimum value the int can be, otherwise it will not be accepted
     * @param maxValue The maximum value the int can be, otherwise it will not be accepted
     */
    public static int selectInt(String headline, int minValue, int maxValue){

        Scanner choice = new Scanner(System.in);
        String result="0";
        boolean runAtLeastOnceBefore=false;

        //This is a failsafe, so the input isn't over the specified amount and also if it is a number (It is 2 checkers & repeaters with 3 error messages)
        do {
            if (Integer.parseInt(result)>maxValue && runAtLeastOnceBefore){
                System.out.println(ANSI_Wrong+"ERROR: Value can't be more then "+maxValue);
            }
            if (Integer.parseInt(result)<=minValue && runAtLeastOnceBefore){
                System.out.println(ANSI_Wrong+"ERROR: Value can't be less then "+minValue);
            }
            System.out.print(ANSI_Usertext+headline + ":");
            System.out.print(ANSI_RESET+" ");
            do {
                if (!TRYFORNUMBER(result)){
                    System.out.println(ANSI_Wrong+"ERROR: Not a value");
                    System.out.print(ANSI_Usertext+headline + ":");
                    System.out.print(ANSI_RESET+" ");
                }
                result = choice.nextLine();
                System.out.println(ANSI_Fill+"");
                runAtLeastOnceBefore=true;
            } while (!TRYFORNUMBER(result));
        } while (Integer.parseInt(result)>maxValue || Integer.parseInt(result)<minValue);

        return Integer.parseInt(result);
    }

    /**
     * <Strong>This is used for inputs from the user, it prints a text and then prompts the user for an answer</Strong>
     * @return This method returns the int, which the user inputted via the scanner
     * @param headline The text you want to print before the userprompt
     */
    public static boolean selectBoolean(String headline){

        Scanner choice = new Scanner(System.in);
        String result="0";
        boolean runAtLeastOnceBefore=false;

        //This is a failsafe, so the input isn't over the specified amount and also if it is a number (It is 2 checkers & repeaters with 3 error messages)
        do {
            if (Integer.parseInt(result)>1 && runAtLeastOnceBefore){
                System.out.println(ANSI_Wrong+"ERROR: Value can't be more then "+1);
            }
            if (Integer.parseInt(result)<=0 && runAtLeastOnceBefore){
                System.out.println(ANSI_Wrong+"ERROR: Value can't be less then "+0);
            }
            System.out.print(ANSI_Usertext+headline + ":");
            System.out.print(ANSI_RESET+" ");
            do {
                if (!TRYFORNUMBER(result)){
                    System.out.println(ANSI_Wrong+"ERROR: Not a value");
                    System.out.print(ANSI_Usertext+headline + ":");
                    System.out.print(ANSI_RESET+" ");
                }
                result = choice.nextLine();
                System.out.println(ANSI_Fill+"");
                runAtLeastOnceBefore=true;
            } while (!TRYFORNUMBER(result));
        } while (Integer.parseInt(result)>1 || Integer.parseInt(result)<0);

        if (Integer.parseInt(result)==1){
            return true;
        } else {
            return false;
        }
    }

    /**
     * <Strong>This is used for checking if the string can be parsed as an int</Strong>
     * @return This method returns the string, if it can be parsed as an int
     * @param possibleValue The value that needs to be checked
     */
    public static boolean TRYFORNUMBER (String possibleValue){
        try {
            Integer.parseInt(possibleValue);
            return true;
        } catch (NumberFormatException ex){
            return false;
        }
    }

}

