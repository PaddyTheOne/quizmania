package com.company;

import java.util.ArrayList;
import java.util.Random;



public class Main {

    //I know that when I put the different values in the class, then they can be read by all the methods, but I have chosen to make the different method dependent on the master method.
    static String[] questions = {
            "- Can an int have the value (Yeds)?",
            "- If an array have a size of 10, can I then add 11 element to it?",
            "- Can you use a value that isn't static and placed in the class section?",
            "- Does a boolean have more then two options when it comes to the value?",
            "- Is the default for a boolean false?",
            "- Can you cast a int as a char?",
            "- Can a String have the value (no123)?",
            "- Can you change the value of a final static int?",
            "- Can you hard cast a double as a int?",
            "- If a int is above its limit does it then start over as the lowest number possible?",
            "- Is a String kinda like a char array?",
            "- Can you use a value defined in a for-loop outside it?",
            "- Does a do-while-loop, go even if the parameter is false?",
            "- Does a while-loop go, even if the parameter is false?",
            "- Can you have more then one parameter in a if-statement?",
            "- Can a float contain the same values as a double?",
            "- If a statement is true, then the if-statement will continue",
            "- A scanner doesn't work unless it has been initiated",
            "- When a char is casted as a int, you get the ASCII-value",
            "- A for-loop(true) always ends"};

    //This array contains all the different right answers
    static boolean[] answers = {false,false,false,false,true,true,true,false,true,true,true,false,true,false,true,false,true,true,true,false};

    //This array has to be out in the class, as it is used globally.
    //It contains all the booleans that tell me if it has been used already
    static boolean[] skip = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};

    //These arraylists contains the players names and their points
    static ArrayList<String> playerNames = new ArrayList<>();
    static ArrayList<Integer> playerPoints = new ArrayList<>();

    //Here I create a random class
    static Random random = new Random();


    public static void main(String[] args) {

        //Amount of questions is set to 10 as that is the standard for 2 ppl
        int amountOfQuestions=10;

        //Here you can set the amount of players to whatever amount you want (min. 2) (max. 4)
        int amountOfPlayers = (int)ScanOptions.selectInt("Set amount of players",2,4);

        //Here you can type in your name. This will run a many times as there are amount of players
        for (int i = 1; amountOfPlayers>=i; i++){
            playerNames.add(ScanOptions.selectString("Type in your name player"+i,1,10));
            playerPoints.add(0);
        }

        //Here I make the game adapt to the amount of players
        switch (amountOfPlayers){
            case (3):
                amountOfQuestions=15;
                break;
            case (4):
                amountOfQuestions=20;
                break;
        }

        //Now The Game Begins
        Program(amountOfPlayers,amountOfQuestions, questions,answers,skip,playerNames,playerPoints);

    }

    public static void Program(int amountOfPlayers, int amountOfQuestions,String[] questions, boolean[] answers ,boolean[] skipTracker, ArrayList<String> playerNames ,ArrayList<Integer> playerPoints){

        int playerTurn=0;
        int questionNum;
        int visualAmountOfQuestions=amountOfQuestions+1;

        //This while-loop continues as long as there is more questions
        while (amountOfQuestions>0){
            //This makes the playerturn loop around, when the last player has used their turn
            if (playerTurn>amountOfPlayers-1){
                playerTurn=0;
            }

            //This prints the players names, so they know whos turn it is
            System.out.println(ScanOptions.ANSI_Usertext+playerNames.get(playerTurn)+"'s turn");

            //This selects a question and returns the questions' location in the questions array
            questionNum=questionPicker(questions,skipTracker);

            //This prints the question and then you get to answer, afterwards it checks if your answer is correct
            System.out.println(ScanOptions.ANSI_Usertext+(visualAmountOfQuestions-amountOfQuestions)+" "+questions[questionNum]);
            if (questionChecker(questionNum,answers,playerPoints,playerNames,ScanOptions.selectBoolean("False = 0 & True = 1"),playerTurn)){
                playerPoints.set(playerTurn,playerPoints.get(playerTurn)+1);
            }

            //This adds 1 to playerturn and removes 1 from amount of questions
            playerTurn++;
            amountOfQuestions--;
        }

        //This part is after the game is over, here it prints the final results
        System.out.println(ScanOptions.ANSI_Fill);
        System.out.println(ScanOptions.ANSI_FinalResult+"--- Final Results ---");
        for (int i = 0; i<playerNames.size();i++){
            System.out.println(ScanOptions.ANSI_FinalResult+playerNames.get(i)+": "+playerPoints.get(i));
        }
        System.out.println(ScanOptions.ANSI_Fill);
    }

    /***
     * <Strong>This is used to pick a new unused question</Strong>
     * @param questions This is the array that contains the different questions
     * @param skipTracker This is the array which contains the boolean that tells the method wether or not the questions have been used before
     * @return This returns the position of the question in the question-array
     */
    public static int questionPicker(String[] questions, boolean[] skipTracker){

        int questionNum;

        //A loop that gives me a random number until it gets a location of a question that hasn't been used
        do {
            questionNum=random.nextInt(20);
        } while (skipTracker[questionNum]!=false);

        //This marks the questions location as "read"
        skip[questionNum]=true;

        return questionNum;
    }

    /***
     * <Strong>This is used to check questions</Strong>
     * @param questionNum This is the location of the question in the question-array
     * @param answers This is the array with answers
     * @param playerPoints This is the arraylist with the players' points
     * @param playerName This is the arraylist with the players' names
     * @param playerAnswer This is what the player answer
     * @param playerTurn This is the number of the player which turn it is
     * @return This returns true if the answer is correct otherwise false
     */
    public static boolean questionChecker(int questionNum, boolean[] answers ,ArrayList<Integer> playerPoints, ArrayList<String> playerName,boolean playerAnswer, int playerTurn){

        //This checks if the answer is correct and prints it. It also ends this method
        if (answers[questionNum]==playerAnswer){
            System.out.println(ScanOptions.ANSI_Correct+"CORRECT!!! - "+playerName.get(playerTurn)+"'s points: "+(playerPoints.get(playerTurn)+1));
            System.out.println(ScanOptions.ANSI_Fill+"");
            return true;
        }

        //This prints if the answer is wrong
        System.out.println(ScanOptions.ANSI_Wrong+"Wrong the answer is: "+answers[questionNum]+" - Your point: "+playerPoints.get(playerTurn));
        System.out.println(ScanOptions.ANSI_Fill+"");

        return false;
    }
}
